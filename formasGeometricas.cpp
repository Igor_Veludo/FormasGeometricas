#include "formas.hpp"

using namespace std;

FormasGeometricas::FormasGeometricas(){

}

FormasGeometricas::FormasGeometricas(float raio){
	this -> raio = raio;
}

FormasGeometricas::FormasGeometricas(float altura, float largura){
	this -> altura = altura;
	this -> largura = largura;
}

FormasGeometricas::~FormasGeometricas(){

}

void FormasGeometricas::setAltura(float altura){
	this -> altura = altura;
}

float FormasGeometricas::getAltura(){
	return altura;
}

void FormasGeometricas::setLargura(float largura){
	this -> largura = largura;
}

float FormasGeometricas::getLargura(){
	return largura;
}

void FormasGeometricas::calculaArea(float altura, float largura){
	return largura*altura;
}

void FormasGeometricas::calculaPerimetro(){
	
}
