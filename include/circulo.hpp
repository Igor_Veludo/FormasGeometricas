#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formasGeometricas.hpp"
#include <string>
#include <iostream>

using namespace std;

class Circulo : public FormasGeometricas{
public:
	Circulo();
	~Circulo();
//	void setRaio(float raio);
//	float getRaio();
	void calculaArea(float raio);
	void calculaPerimetro(float raio);
};

#endif