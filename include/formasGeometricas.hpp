#ifndef FORMASGEOMETRICAS_HPP
#define FORMASGEOMETRICAS_HPP

#include <string>
#include <iostream>

using namespace std;

class FormasGeometricas{
private:
	float altura, largura, raio;

public:
	FormasGeometricas();
	FormasGeometricas(float raio);
	FormasGeometricas(float altura, float largura);
	~FormasGeometricas();
	void setAltura(float altura);
	float getAltura();
	void setLargura(float largura);
	float getLargura();
	void setRaio(float raio);
	float getRaio();
	float calculaArea(float altura, float largura);
	void calculaPerimetro();
};

#endif