#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formasGeometricas.hpp"
#include <string>
#include <iostream>

using namespace std;

class Quadrado : public FormasGeometricas{
public:
	Quadrado();
	~Quadrado();
//	void setRaio(float raio);
//	float getRaio();
	void calculaArea(float altura);
	void calculaPerimetro(float altura);
};

#endif