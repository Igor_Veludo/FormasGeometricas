#include "quadrado.hpp"
#include "circulo.hpp"
#include <iostream>
#include <string>

using namespace std;

int main(){
	Circulo *circulo = new Circulo();
	Quadrado *quadrado = new Quadrado();
	float valor;
	cin >> valor;

	circulo->calculaArea(valor);
	circulo->calculaPerimetro(valor);

	quadrado->calculaArea(valor);
	quadrado->calculaPerimetro(valor);
	
	return 0;
}
